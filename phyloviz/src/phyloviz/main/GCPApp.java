package phyloviz.main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import phyloviz.core.data.AbstractProfile;
import phyloviz.core.data.TypingData;
import phyloviz.matrix.dissimilarity.MatrixPairwiseDissimilarity;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.matrix.dissimilarity.ProfilePairwiseDissimilarity;
import phyloviz.mlst.MLSTypingFactory;
import phyloviz.project.ProjectItem;
import phyloviz.project.action.SaveAsProjectAction;
import phyloviz.snp.SNPFactory;
import phyloviz.upgma.GCPClusteringMethod;
import phyloviz.upgma.algorithm.completelinkage.CLMethodProvider;
import phyloviz.upgma.algorithm.singlelinkage.SLMethodProvider;
import phyloviz.upgma.algorithm.upgma.UPGMAMethodProvider;
import phyloviz.upgma.algorithm.wpgma.WPGMAMethodProvider;
import phyloviz.upgma.run.UPGMARunner;
import phyloviz.upgmanjcore.output.json.JsonFormat;
import phyloviz.upgmanjcore.output.newick.NewickFormat;

public class GCPApp {

	private static Options getOptions() {
		Options options = new Options();

		options.addOption(Option.builder("m").longOpt("method").hasArg().valueSeparator().argName("method")
				.desc("Computes a tree using a distance algorithm. You may choose this method from: (U)PGMA (default), (W)PGMA, (S)L or (C)L.")
				.build())
		.addOption(Option.builder("O").longOpt("output-format").hasArg().argName("format")
				.desc("Specify the output data file format. You may choose this format from: (N)ewick (default) or (J)SON.")
				.build())
		.addOption(Option.builder("t").longOpt("typing-method").hasArg().argName("method")
				.desc("Specify the input data file format. You may choose this method from: (D)istance matrix, (M)LST (default), (S)NP")
				.build())
		.addOption(Option.builder("o").longOpt("output-tree").hasArg().argName("output tree file")
				.desc("Will write the infered tree into the output tree file.")
				.build())
		.addOption(Option.builder("i").longOpt("input-data").hasArg().argName("input data file")
				.desc("The input data file contains typing data or a distance matrix.")
				.build())
		.addOption(Option.builder("h").longOpt("help").desc("Displays this usage.")
				.build());
		return options;
	}

	public static void main(String[] args)
			throws InstantiationException, IllegalAccessException, ParseException, IOException {

		CommandLineParser parser = new DefaultParser();
		Options options = getOptions();

		CommandLine cmd = parser.parse(options, args);

		if (cmd.hasOption("h")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(GCPApp.class.getSimpleName(), options);
		} else {
			
			if(!cmd.hasOption("i")){
				System.err.println("Must use option -i to define your input data file.");
				return;
			}
			
			String in = cmd.getOptionValue("i");
			String out = cmd.hasOption("o") ? cmd.getOptionValue("o") : in.substring(in.lastIndexOf("/")+1, in.lastIndexOf('.'));

			FileReader fr = new FileReader(new File(System.getProperty("user.dir"), in));
			PairwiseDissimilarity pd = null;
			ProjectItem result = null;
			TypingData<? extends AbstractProfile> td = null;
			if (cmd.hasOption("t")) {
				switch (cmd.getOptionValue("t")) {
				case "D":
					pd = new MatrixPairwiseDissimilarity<>(fr);
					break;
				case "M":
					td = new MLSTypingFactory().loadData(fr);
					pd = new ProfilePairwiseDissimilarity<>(td);
					break;
				case "S":
					td = new SNPFactory().loadData(fr);
					pd = new ProfilePairwiseDissimilarity<>(td);
					break;
				}
			} else {
				pd = new ProfilePairwiseDissimilarity<>(new MLSTypingFactory().loadData(fr));
			}
			
			GCPClusteringMethod method = (GCPClusteringMethod) new UPGMAMethodProvider().getMethod();
			if (cmd.hasOption("m")) {
				String val = cmd.getOptionValue("m");
				switch (val) {
				case "U": {
					out += ".upgma";
					break;

				}
				case "W": {
					out += ".wpgma";
					method = (GCPClusteringMethod) new WPGMAMethodProvider().getMethod();
					break;
				}
				case "S": {
					out += ".sl";
					method = (GCPClusteringMethod) new SLMethodProvider().getMethod();
					break;
				}
				case "C": {
					out += ".cl";
					method = (GCPClusteringMethod) new CLMethodProvider().getMethod();
					break;
				}
				}
			} else {
				out += ".upgma";
			}

			result = new UPGMARunner(method, pd).run(td);

			SaveAsProjectAction saver = new SaveAsProjectAction();
			if (cmd.hasOption("O")) {
				switch (cmd.getOptionValue("O")) {
				case "J":
					saver.save(new JsonFormat(), out, result);
					break;
				case "N":
					saver.save(new NewickFormat(), out, result);
					break;

				}
			} else {
				saver.save(new NewickFormat(), out, result);
			}
		}
	}
}
