package phyloviz.goeburst.output;

import java.util.Collection;

import phyloviz.goeburst.cluster.GOeBurstClusterWithStats;
import phyloviz.matrix.Identifiable;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.upgmanjcore.output.INodeSaver;

public class GOeBurstSaver implements INodeSaver {

	private Collection<GOeBurstClusterWithStats> clustering;
	PairwiseDissimilarity<? extends Identifiable> pd;

	public GOeBurstSaver(Collection<GOeBurstClusterWithStats> save, PairwiseDissimilarity<? extends Identifiable> pairwiseDissimilarity){
		this.clustering = save;
		this.pd = pairwiseDissimilarity;
	}
	
	@Override
	public Object get() {
		return new Object[]{clustering, pd};
	}

}
