/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */

package phyloviz.goeburst.run;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import phyloviz.core.data.AbstractProfile;
import phyloviz.core.data.TypingData;
import phyloviz.goeburst.GOeBurstComparator;
import phyloviz.goeburst.algorithm.MSTAlgorithm;
import phyloviz.goeburst.cluster.Edge;
import phyloviz.goeburst.tree.GOeBurstMSTResult;
import phyloviz.goeburst.tree.GOeBurstNode;
import phyloviz.matrix.Identifiable;
import phyloviz.matrix.Matrix;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.project.ProjectItem;

public class MSTRunner {
	private final int MAX_LEVEL = 7;

	private Matrix<GOeBurstNode> m;
	private GOeBurstComparator<GOeBurstNode> cmp;
	static int times = 0;

	public MSTRunner(PairwiseDissimilarity<Identifiable> pd) {
		List<GOeBurstNode> nodes = new ArrayList<>();
		for (int i = 0; i < pd.size(); i++) {
			nodes.add(new GOeBurstNode(pd.get(i)));
		}
		this.cmp = new GOeBurstComparator<GOeBurstNode>(pd, MAX_LEVEL);
		this.m = new Matrix<GOeBurstNode>(nodes, pd, cmp);
	}

	public ProjectItem run(TypingData<? extends AbstractProfile> td) {

		int maxLV = cmp.maxLevel();

		Iterator<GOeBurstNode> in = m.iterator();
		while (in.hasNext()) {
			GOeBurstNode n = in.next();
			n.updateLVs(m, maxLV);
		}

		Collections.sort(m.getNodes(), m.getComparator().getProfileComparator());

		Collection<Edge<GOeBurstNode>> tree = null;
		if (m.size() > 1) {
			MSTAlgorithm<GOeBurstNode> algorithm = new MSTAlgorithm<GOeBurstNode>(m);
			tree = algorithm.getTree();
		}
		return new GOeBurstMSTResult(tree, m, maxLV);
	}
}
