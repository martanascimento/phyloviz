/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package phyloviz.goeburst.cluster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeMap;

import phyloviz.algo.util.DisjointSet;
import phyloviz.goeburst.algorithm.GOeBurstWithStats;
import phyloviz.matrix.Matrix;

public class GOeBurstClusterWithStats extends GOeBurstCluster {

    private final int[] maxLVs;
    private final TreeMap<Edge<GOeBurstNodeExtended>, EdgeInfo> eInfoMap;
    private final EdgeTieStats stat;
    private final ArrayList<Edge<GOeBurstNodeExtended>> SLVedges;
    private final GOeBurstWithStats mInstance;
    protected GOeBurstNodeExtended fakeRoot;

    public GOeBurstClusterWithStats(GOeBurstWithStats algInstance, Matrix<GOeBurstNodeExtended> m) {
        super(m);
        mInstance = algInstance;
        maxLVs = new int[MAXLV + 1];
        eInfoMap = new TreeMap<Edge<GOeBurstNodeExtended>, EdgeInfo>();
        stat = new EdgeTieStats();
        SLVedges = new ArrayList<Edge<GOeBurstNodeExtended>>();
        fakeRoot = null;
    }

    @Override
    public void setID(int id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    public GOeBurstNodeExtended getFakeRoot() {
        return fakeRoot;
    }

    public int getMaxXLV(int level) {
        if (level > MAXLV || level < 0) {
            level = MAXLV;
        }

        return maxLVs[level];
    }

    public void setMaxXLV(int level, int value) {
        if (level > MAXLV || level < 0) {
            return;
        }

        maxLVs[level] = value;
    }

    public void addEdge(Edge<GOeBurstNodeExtended> e) {
        addNode(e.getU());
        addNode(e.getV());
        edges.add(e);
        if (m.getDistance(e.getU(), e.getV()) == 1) {
            SLVedges.add(e);
        }
    }

    @Override
    public void add(Edge<GOeBurstNodeExtended> e) {
        add(e.getU());
        add(e.getV());
        edges.add(e);
        if (m.getDistance(e.getU(), e.getV()) == 1) {
            SLVedges.add(e);
        }
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Collection<GOeBurstNodeExtended> getSTs() {
        return nodes;
    }

    @Override
    public int getIsolates() {
        return isolates;
    }

    @Override
    public ArrayList<Edge<GOeBurstNodeExtended>> getEdges() {
        return edges;
    }

    public int compareTo(GOeBurstClusterWithStats g) {
        return g.size() - size();
    }

    public boolean equals(GOeBurstClusterWithStats g) {
        return compareTo(g) == 0;
    }

    @Override
    public String toString() {
        return " " + Integer.toString(id) + " ";
    }

    @Override
    public void updateVisibleEdges() {

        // Kruskal's algorithm.
        Collections.sort(getEdges(), m.getComparator().getEdgeComparator());
        DisjointSet s = new DisjointSet(maxStId);
        Iterator<Edge<GOeBurstNodeExtended>> iter = getEdges().iterator();
        visibleEdges = 0;

        while (iter.hasNext()) {
        	Edge<GOeBurstNodeExtended> e = iter.next();
            e.setVisible(false);

            if (!s.sameSet(e.getU().getUID(), e.getV().getUID())) {
                s.unionSet(e.getU().getUID(), e.getV().getUID());
                e.setVisible(true);
                visibleEdges++;
            }
        }

        fakeRoot = null;
    }

    public void updateVisibleEdges(Edge<GOeBurstNodeExtended> edge) {
        this.visibleEdges += edge.visible() ? 1 : 0;
    }

    @Override
    public void updateVisibleEdges(GOeBurstNodeExtended root) {
        int[] orig = new int[MAXLV + 1];

        for (int i = 0; i < MAXLV + 1; i++) {
            orig[i] = root.getLV(i);
            root.setLV(i, Integer.MAX_VALUE);
        }

        updateVisibleEdges();

        for (int i = 0; i < MAXLV + 1; i++) {
            root.setLV(i, orig[i]);
        }
    }

    @Override
    public int getVisibleEdges() {
        return visibleEdges;
    }

    public int[] getEdgeTieStatsNE() {
        return stat.ne;
    }

    public int[]  getEdgeTieStatsFNE() {
        return stat.fne;
    }

    public int[]  getEdgeTieStatsXlv() {
        return stat.xLV;
    }

    public int getEdgeTieStatsWithoutTies() {
        return stat.withoutTies;
    }

    public void setEdgeTieStatsNE(int idx, int value) {
        stat.ne[idx] = value;
    }

    public void setEdgeTieStatsFNE(int idx, int value) {
        stat.fne[idx] = value;
    }

    public void setEdgeTieStatsXlv(int idx, int value) {
        stat.xLV[idx] = value;
    }

    public void setEdgeTieStatsWithoutTies(int value) {
        stat.withoutTies = value;
    }

    public void setEdgeMaxTieLevel(Edge<GOeBurstNodeExtended> e, int maxTie) {
        EdgeInfo info = eInfoMap.get(e);
        if (info == null) {
            info = new EdgeInfo();
            eInfoMap.put(e, info);
        }
        info.maxTie = maxTie;
    }

    public int getEdgeMaxTieLevel(Edge<GOeBurstNodeExtended> e) {
        EdgeInfo info = eInfoMap.get(e);
        if (info != null) {
            return info.maxTie;
        }

        return 0;
    }

    public void updateMaxLVs(GOeBurstNodeExtended u) {
        // Update maxLVs info.
        int i, j;

        for (i = 0; i < MAXLV && u.getLV(i) == maxLVs[i]; i++);
        if (i < MAXLV && u.getLV(i) >= maxLVs[i]) {
            for (j = 0; j < MAXLV + 1; j++) {
                maxLVs[j] = u.getLV(j);
            }
        }

//        System.out.println("maxLV: " + maxLVs[0]);
    }

    public boolean isFounder(GOeBurstNodeExtended st) {
        int i;
        for (i = 0; i < MAXLV && st.getLV(i) == maxLVs[i]; i++);
        return i >= MAXLV;
    }

    static private class EdgeInfo {

        public int maxTie = 0;
        //public int nbTies = 0;
        public String info = "";
    }

    static private class EdgeTieStats {

        public int[] ne;
        public int[] fne;
        public int withoutTies;
        public int[] xLV;

        public EdgeTieStats() {
            withoutTies = 0;
            xLV = new int[MAXLV + 3];
            ne = new int[MAXLV];
            fne = new int[MAXLV];
        }
    }

    public void computeStatistics() {
        Iterator<Edge<GOeBurstNodeExtended>> iter = this.getEdges().iterator();

        while (iter.hasNext()) {
        	Edge<GOeBurstNodeExtended> e = iter.next();

            stat.ne[(int) (m.getDistance(e.getU(), e.getV()) - 1)]++;

            if (!e.visible()) {
                continue;
            }

            getInfo(e);
        }
    }

    public int getSTxLV(GOeBurstNodeExtended st, int lv) {
        return mInstance.getSTxLV(st.getIdentifiable(), lv);
    }

    public String getInfo(GOeBurstNodeExtended st) {
        String s = "";

        s += "# SLVs = " + st.getLV(0) + " ( " + mInstance.getSTxLV(st.getIdentifiable(), 0) + " )";
        s += "\n# DLVs = " + st.getLV(1) + " ( " + mInstance.getSTxLV(st.getIdentifiable(), 1) + " )";
        s += "\n# TLVs = " + st.getLV(2) + " ( " + mInstance.getSTxLV(st.getIdentifiable(), 2) + " )";
        s += "\n# SAT  = " + st.getLV(3) + " ( " + mInstance.getSTxLV(st.getIdentifiable(), 3) + " )";
        s += "\n# isolates = " + st.getIdentifiable().getFreq();
        s += "\n";

        return s;
    }

    public String getInfo(Edge<GOeBurstNodeExtended> e) {

        if (m.getDistance(e.getU(), e.getV()) != 1) {
            return "";
        }

        EdgeInfo info = new EdgeInfo();
        eInfoMap.put(e, info);

        // Compute ties.
        info.info = "";
        // Find cut for 'e'.
        DisjointSet cut = new DisjointSet(maxStId);
        Iterator<Edge<GOeBurstNodeExtended>> niter = SLVedges.iterator();
        while (niter.hasNext()) {
        	Edge<GOeBurstNodeExtended> f = niter.next();

            if (f.visible() && f != e && !cut.sameSet(f.getU().getUID(), f.getV().getUID())) {
                cut.unionSet(f.getU().getUID(), f.getV().getUID());
            }
        }

        // Find ties.
        int maxtb = 0;
        int nt = 0;
        niter = SLVedges.iterator();
        while (niter.hasNext()) {
        	Edge<GOeBurstNodeExtended> f = niter.next();

            if (!f.visible() && m.getDistance(f.getU(), f.getV()) == m.getDistance(e.getU(), e.getV())
                    && cut.findSet(f.getU().getUID()) != cut.findSet(f.getV().getUID())) {
                info.info += " + " + f.getU().getID() + " -- " + f.getV().getID() + " ";

                nt++;


            }
        }

        stat.fne[(int) (m.getDistance(e.getU(), e.getV()) - 1)]++;

        if (nt == 0) {
            info.info += "0 ties\n";
            stat.withoutTies++;
            return info.info;
        }

        info.info += nt + " ties (highest ";
        if (maxtb / 2 == MAXLV + 1) {
            info.info += "tiebreak at freq)\n";
        } else if (maxtb / 2 == MAXLV + 2) {
            info.info += "tiebreak at id)\n";
        } else if (maxtb / 2 == MAXLV) {
            info.info += "tiebreak at sat)\n";
        } else {
            int c = (1 + maxtb / 2);
            info.info += "tiebreak at " + ((c == 1) ? 's' : ((c == 2) ? 'd' : 't')) + "lv)\n";
        }

        info.maxTie = maxtb / 2 + 1;

        if(info.maxTie - 1 == 9)
//        	System.out.println();
        stat.xLV[info.maxTie - 1]++;

        return info.info;
    }
}
