package phyloviz.matrix;

import java.util.Comparator;

public interface OTUComparator<T extends OTU> extends Comparator<T>{

	public int compare(T t1, T t2);
	public int compare(TaxaPair<T> t);
	public Comparator<TaxaPair<T>> getEdgeComparator();
	public Comparator<T> getProfileComparator();
	public int maxLevel();
	
}
