package phyloviz.matrix;

public class DistanceVector implements Identifiable{
	
	protected final int uid;
	protected final String id;
	public float[] distances;
	
	public DistanceVector(int uid, String id, int size){
		this.uid = uid;
		this.id = id;
		this.distances = new float[size];
	}
	@Override
	public int getUID(){
		return this.uid;
	}
	@Override
	public String getID(){
		return this.id;
	}
	@Override
	public int getFreq() {
		return 1;
	}
	public void setDistances(float[] d){
		this.distances = d;
	}
	
	@Override
	public String toString(){
		return this.id;
	}
}
