package phyloviz.matrix.dissimilarity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import phyloviz.algo.AbstractDistance;
import phyloviz.core.data.AbstractProfile;
import phyloviz.core.data.TypingData;
import phyloviz.upgmanjcore.distance.HammingDistanceProvider;

public class ProfilePairwiseDissimilarity<T extends AbstractProfile> implements PairwiseDissimilarity<T> {
	
	private AbstractDistance<T> ad;
	private final List<T> profiles = new ArrayList<T>();

	public ProfilePairwiseDissimilarity(TypingData<T> td, AbstractDistance<T> ad) {
		this.ad = ad;
		for (Iterator<T> it = td.iterator(); it.hasNext();) {
			profiles.add(it.next());			
		}
	}

	public ProfilePairwiseDissimilarity(TypingData<T> td) {
		this(td, new HammingDistanceProvider<T>().getDistance());
	}

	@Override
	public int size() {
		return profiles.size();
	}

	@Override
	public List<T> getIdentifiable() {
		return profiles;
	}

	@Override
	public float distance(T t1, T t2) {
		return ad.level(t1, t2);
	}

	@Override
	public T get(int i) {
		return profiles.get(i);
	}

//	public AbstractDistance<T> getDistanceProvider() {
//		return ad;
//	}

//	@Override
//	public int maxLevel() {
//		return ad.maxLevel();
//	}

//	@Override
//	public void setMaxLevel(int lv) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public AbstractDistance getDistanceProvider() {
//		return ad;
//	}
//
//	@Override
//	public void setDistanceProvider(AbstractDistance ad) {
//		this.ad = ad;
//		
//	}

//	public void setDistanceProvider(AbstractDistance<T> ad) {
//		this.ad = ad;
//		
//	}

}
