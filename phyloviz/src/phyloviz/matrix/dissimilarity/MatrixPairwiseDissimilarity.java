package phyloviz.matrix.dissimilarity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import phyloviz.matrix.DistanceVector;

public class MatrixPairwiseDissimilarity<T extends DistanceVector> implements PairwiseDissimilarity<T> {

	private List<T> something;
	private float[][] distances;
//	private int maxLevel;

	public MatrixPairwiseDissimilarity(Reader r) {
		int size = 0;
		try (BufferedReader in = new BufferedReader(r)) {
			if (in.ready())
				size = Integer.parseInt(in.readLine());

			something = new ArrayList<T>(size);
			distances = new float[size][size];

			String s = in.readLine();
			String[] line = s.split("[ ,\t]+", 0);
			BiConsumer<Integer, String[]> parser = this::parse;
			if (line.length == size)
				parser = this::parseUpper;

			int uid = 0;
			parser.accept(uid, line);
			something.add((T) new DistanceVector(uid++, line[0], size));
			while (in.ready()) {
				s = in.readLine();
				
				line = s.split("[ ,\t]+", 0);

				parser.accept(uid, line);

				something.add((T) new DistanceVector(uid++, line[0], size));
			}
			
			for(int i = 0; i<distances.length; i++){
				something.get(i).setDistances(distances[i]);
				distances[i] = null;
			}
			distances = null;
			
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseUpper(int id, String[] line) {
		for (int i = 1; i < line.length; i++) {
			distances[id][id + i] = Float.parseFloat(line[i]);
			distances[id + i][id] = distances[id][id + i];
		}
	}

	private void parse(int id, String[] line) {
		for (int i = 1; i < line.length; i++) {
			distances[id][i - 1] = Float.parseFloat(line[i]);
			distances[i - 1][id] = distances[id][i - 1];
		}
	}

	@Override
	public float distance(DistanceVector t1, DistanceVector t2) {
		return t1.distances[t2.getUID()];
	}

	@Override
	public int size() {
		return something.size();
	}

	@Override
	public T get(int i) {
		return something.get(i);
	}

	@Override
	public List<T> getIdentifiable() {
		return something;
	}

//	@Override
//	public int maxLevel() {
//		return this.maxLevel;
//	}
//
//	@Override
//	public void setMaxLevel(int lv) {
//		this.maxLevel = lv;
//
//	}

}
