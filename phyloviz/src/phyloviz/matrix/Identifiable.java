package phyloviz.matrix;

public interface Identifiable {

	public int getUID();
	public String getID();
	public int getFreq();
}
