package phyloviz.matrix;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.upgmanjcore.tree.Index;

public class Matrix<T extends OTU> {

	private List<T> nodes;
	private Index dummy;
	private int size;
	private final OTUComparator<T> cmp;
	private final PairwiseDissimilarity<Identifiable> pd;

	public Matrix(List<T> nodes, PairwiseDissimilarity<Identifiable> pd, OTUComparator<T> cmp) {
		this.size = nodes.size();
		this.nodes = new ArrayList<>(nodes);
		this.dummy = new Index(-1);
		this.pd = pd;
		this.cmp = cmp;
		
		for (int i = 0; i < size; i++) {
			Identifiable p1 = pd.get(i);;
			T curr = nodes.get(i);
			curr.addIndex(newIndex(i), size);
			for (int j = i + 1; j < size; j++) {
				Identifiable p2 = pd.get(j);
				curr.setDistance(p2.getUID(), pd.distance(p1, p2));
			}
		}

	}
	private Index newIndex(int i) {
		Index index = new Index(i);
		index.next = dummy;
		dummy.prev.next = index;
		index.prev = dummy.prev;
		dummy.prev = index;
		return index;
	}
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Index current = dummy;

			@Override
			public boolean hasNext() {
				return current.next != dummy;
			}

			@Override
			public T next() {
				current = current.next;
				return nodes.get(current.index);
			}

		};
	}

	public int size() {
		return this.size;
	}

	public void clean() {
		nodes = null;
		dummy = null;
	}
	public float getDistance(TaxaPair<T> edge){
		return getDistance(edge.getU(), edge.getV());
	}
	public float getDistance(T t1, T t2) {
		if (t1.getNodeIdx() < t2.getNodeIdx())
			return t1.getDistance(t2.indexes.index);
		else if(t1.getNodeIdx() > t2.getNodeIdx()) 
			return t2.getDistance(t1.indexes.index);
		return 0;
	}

	public void setDistance(T t1, T t2, float distance) {
		if (t1.getNodeIdx() < t2.getNodeIdx()){
			t1.setDistance(t2.indexes.index, distance);
		}
		else{
			t2.setDistance(t1.indexes.index, distance);
		}
	}
	
	public void remove(T t) {
				
		nodes.set(t.indexes.index, null);
		
		t.indexes.prev.next = t.indexes.next;
		t.indexes.next.prev = t.indexes.prev;

		t.empty();

		size--;

	}
	public void add(T toAdd) {
		
		Index index = newIndex(dummy.prev.index + 1);
		toAdd.addIndex(index, size +1);
		nodes.add(toAdd);
		
		Iterator<T> iter = this.iterator();
		while(iter.hasNext()){
			T next = (T) iter.next();
			if(!next.equals(toAdd)){
				float d = pd.distance(toAdd.getIdentifiable(), next.getIdentifiable());
				setDistance(toAdd, next, d);
			}
		}
		
		this.size++;
	}
	public void replace(T pos, T toAdd) {

		toAdd.addIndex(pos.indexes, pd.size());
		
		nodes.set(pos.indexes.index, toAdd);
//		nodes.add(t2.indexes.index, null);
//		remove(t2);

	}

	public T get(int index) {
		return nodes.get(index);
	}
	
	public int compare(T t1, T t2){
		return cmp.compare(t1, t2);
	}
	public int compare(TaxaPair<T> p){
		return cmp.compare(p);
	}
	public OTUComparator<T> getComparator(){
		return this.cmp;
	}
//	public Comparator<? super TaxaPair<T>> getEdgeComparator() {
//		return this.cmp.getEdgeComparator();
//	}
//	public Comparator<T> getProfileComparator() {
//		return this.cmp.getProfileComparator();
//	}

	public PairwiseDissimilarity<Identifiable> getPairwiseDissimilarity(){
		return pd;
	}
	public List<T> getNodes() {
		return this.nodes;
	}
}
