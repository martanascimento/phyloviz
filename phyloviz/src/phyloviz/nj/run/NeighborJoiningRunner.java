/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.nj.run;

import phyloviz.matrix.Identifiable;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.nj.MEClusteringMethod;
import phyloviz.nj.NeighborJoiningItem;
import phyloviz.nj.algorithm.NJ;
import phyloviz.nj.tree.NJRoot;
import phyloviz.project.ProjectItem;

/**
 *
 * @author Adriano
 */
public class NeighborJoiningRunner{
    
    private final MEClusteringMethod method;
	private PairwiseDissimilarity<Identifiable> pd;

    public NeighborJoiningRunner(MEClusteringMethod method, PairwiseDissimilarity<Identifiable> pd) {
		this.method = method;
		this.pd = pd;
	}

	public ProjectItem run() {
        
        NJ nj = method.getClusteringMethod(pd);
        
        NJRoot root = nj.execute();

        return new NeighborJoiningItem(root, method);
    }
}
