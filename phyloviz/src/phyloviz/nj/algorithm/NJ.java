/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.nj.algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import phyloviz.matrix.Identifiable;
import phyloviz.matrix.Matrix;
import phyloviz.matrix.TaxaPair;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.nj.tree.NJLeafNode;
import phyloviz.nj.tree.NJNode;
import phyloviz.nj.tree.NJRoot;
import phyloviz.nj.tree.NJUnionNode;


public abstract class NJ {

	protected int unionID;
	protected Matrix<NJNode> m;

	public NJ(PairwiseDissimilarity<Identifiable> pd) {
		this.unionID = pd.size();

		List<NJNode> nodes = new ArrayList<>();
		for(int i = 0; i < pd.size(); i++){
			nodes.add(new NJLeafNode(pd.get(i)));
		}
		
		this.m = new Matrix<NJNode>(nodes, pd, null);
		
		for (int j = 1; j < nodes.size(); j++) {
			nodes.get(j).updateSum(nodes);
		}
	}

	public NJRoot execute() {

		while (m.size() > 2) {

			TaxaPair<NJNode> w = select();

			NJUnionNode union = join(w);
			m.replace(union.t1, union);
			

			reduce(union);
			m.remove(union.t2);
		}
		Iterator<NJNode> iter = m.iterator();
		NJNode nt1 = iter.next();
		NJNode nt2 = iter.next();
		NJRoot root = new NJRoot(nt1, nt2, m.getDistance(nt1, nt2));
		m.clean();

		return root;
	}

	private void reduce(NJUnionNode union) {
		float dst = m.getDistance(union.t1, union.t2);
		Iterator<NJNode> iter = m.iterator();
		while (iter.hasNext()) {
			NJNode curr = iter.next();
		
			if (curr.getUID() == union.getUID() || curr.getUID() == union.t1.getUID() || curr.getUID() == union.t2.getUID())
				continue;

			float d_ik = m.getDistance(curr, union.t1);
			float d_jk = m.getDistance(curr, union.t2);
			
			float newDistance = getReductionCriterion(union, d_ik, d_jk);//(distanceX_U1 + distanceY_U1 - dst) / 2;
			
			if (curr.getNodeIdx() < union.getNodeIdx()) {
				curr.removeDistance(union.t1.index, union.t2.index);
				curr.setDistance(union.t1.index, newDistance);
				union.updateSum(newDistance);
			} else {
				union.setDistance(curr.index, newDistance);
				curr.updateSum(newDistance - d_ik - d_jk);
			}
		}
	}

	private TaxaPair<NJNode> select() {
		TaxaPair<NJNode> pair = null;
		float minQ = Float.POSITIVE_INFINITY;
		
		Iterator<NJNode> iter = m.iterator();
		while (iter.hasNext()) { // for each line in this column, calculates Q
			NJNode nt = iter.next();
			nt.calculateQ(this);
			if (nt.minQ < minQ) {
				pair = new TaxaPair<NJNode>(nt, m.get(nt.minIndex));
				minQ = nt.minQ;
			}
		}
		return pair;
	}

	public abstract float calculateQ(int nodeIdx, int l);
	protected abstract NJUnionNode join(TaxaPair<NJNode> w);
	protected abstract float getReductionCriterion(NJUnionNode union, float d_ik, float d_jk);

}
