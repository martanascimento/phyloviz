/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.nj.algorithm.saitou_nei;

import phyloviz.matrix.Identifiable;
import phyloviz.matrix.TaxaPair;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.nj.algorithm.NJ;
import phyloviz.nj.tree.NJNode;
import phyloviz.nj.tree.NJUnionNode;

/**
 *
 * @author Adriano
 */
public class NJSaitouNei extends NJ {

	public NJSaitouNei(PairwiseDissimilarity<Identifiable> pd) {
		super(pd);
	}
	@Override
    public float calculateQ(int nodeIdx, int l) {
		
		float r = m.size();
    	float d_ij =  m.getDistance(m.get(nodeIdx), m.get(l));
    	
    	float sum_i = ((NJNode) m.get(nodeIdx)).getDistancesSum();
    	float sum_j = ((NJNode) m.get(l)).getDistancesSum();
    	
    	float q_ij = (r - 2)*d_ij - sum_i - sum_j;
    	return q_ij;
    }
	
	@Override
    protected NJUnionNode join(TaxaPair<NJNode> w) {
		float d_ij = m.getDistance(w);
        float sum_i = ((NJNode) w.getU()).getDistancesSum();
        float sum_j = ((NJNode) w.getV()).getDistancesSum();
        		 
        float d_iu = (0.5f * d_ij) + (1/(2 * m.size()-2)) * (sum_i - sum_j); 
        float d_ju = d_ij - d_iu;
        
        return new NJUnionNode(unionID, String.valueOf(unionID++), (NJNode)w.getU(), (NJNode)w.getV(), d_iu, d_ju);
    }
	@Override
	protected float getReductionCriterion(NJUnionNode union, float d_ik, float d_jk) {
		float lambda = 0.5f;
		float d_iu = 0;
		float d_ju = 0;
		
		float d_uk = lambda * (d_ik - d_iu) + (1 - lambda) * (d_jk - d_ju); 
		
		return d_uk;
	}
}
