package phyloviz.nj.output.json;

import java.util.LinkedHashMap;
import java.util.Map;

import phyloviz.matrix.OTU;
import phyloviz.nj.tree.NJLeafNode;
import phyloviz.nj.tree.NJRoot;
import phyloviz.nj.tree.NJUnionNode;
import phyloviz.upgmanjcore.output.INodeSaver;
import phyloviz.upgmanjcore.output.json.JsonWriter;

public class NJJsonWriter extends JsonWriter{

	private StringBuilder leaves = new StringBuilder();
	private StringBuilder unions = new StringBuilder();
	
	
	@Override
	protected Map<String, String> getObjects(INodeSaver n) {
		Map<String, String> map = new LinkedHashMap<>();
		NJRoot root = (NJRoot) n.get();
		
		write(root.getNodeLeft());
		write(root.getNodeRight());
		
        map.put("leaf", leaves.toString());
        map.put("union", unions.toString());
        map.put("root", format(root));
        return map;
	} 
	
	
	private void add(StringBuilder sb, String s){
		sb.append(s);
	}
	private String format(NJRoot root){
		String l = root.getNodeLeft().getID();
		String r = root.getNodeRight().getID();
		return "\t\t{\"distance\": "+ root.getDistance() + ", \"left\": \"" + l + "\", \"right\": \"" + r + "\"},\n";
	}
	private void write(OTU n) {
		if (!n.isLeaf()) {
			NJUnionNode node = (NJUnionNode) n;
			OTU l = node.t1;
			OTU r = node.t2;
			
			write(l); write(r);
			add(unions, format(node));

		} else {
			NJLeafNode node = (NJLeafNode) n;
			add(leaves,format(node));
		}
	}
    private String format(NJLeafNode leaf){
    	String id = leaf.getID();
    	String profile = leaf.getIdentifiable().getID();
        return "\t\t{\"id\": \"" + id + "\", \"profile\": \"" + profile + "\"},\n";
    }
    private String format(NJUnionNode union){
    	
    	String id = union.getID();
    	float distL = union.getDistanceLeft();
    	float distR = union.getDistanceRight();
    	String leftId = union.t1.getID();
    	String rightId = union.t2.getID();
    	
    	return "\t\t{\""
                + "id\": \"" + id + "\", \""
                + "left\": \"" + leftId + "\", \"" 
                + "distanceLeft\": " + distL + ", \""
                + "right\": \"" + rightId + "\", \""
                + "distanceRight\": " + distR + "},\n";
    }  
}
