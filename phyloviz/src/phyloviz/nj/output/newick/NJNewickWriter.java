package phyloviz.nj.output.newick;

import phyloviz.nj.tree.NJLeafNode;
import phyloviz.nj.tree.NJNode;
import phyloviz.nj.tree.NJRoot;
import phyloviz.nj.tree.NJUnionNode;
import phyloviz.upgmanjcore.output.INodeSaver;
import phyloviz.upgmanjcore.output.newick.NewickWriter;

public class NJNewickWriter extends NewickWriter {

	@Override
	public String write(INodeSaver n) {
		NJRoot root = (NJRoot) n.get();

		NJNode l = (NJNode) root.getNodeLeft();
		NJNode r = (NJNode) root.getNodeRight();

		StringBuilder sb = new StringBuilder();

		sb.append("(").append(format(root, l)).append(",").append(format(root, r)).append(");");

		return sb.toString();
	}

	private String format(NJNode parent, NJNode n) {
		if (!n.isLeaf()) {
			NJUnionNode node = (NJUnionNode) n;
			NJNode l = node.t1;
			NJNode r = node.t2;

			StringBuilder sb = new StringBuilder();

			sb.append("(").append(format(n, l)).append(",").append(format(n, r)).append(")");
			sb.append(":").append(((NJUnionNode)parent).getDistance(node));
			return sb.toString();
		} else {
			return ((NJLeafNode) n).getID() + ":" + ((NJUnionNode)parent).getDistance(n);
		}
	}

}
