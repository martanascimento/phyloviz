/*-
 * Copyright (c) 2011, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 * 
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent modules,
 * and to copy and distribute the resulting executable under terms of your
 * choice, provided that you also meet, for each linked independent module,
 * the terms and conditions of the license of that module.  An independent
 * module is a module which is not derived from or based on this library.
 * If you modify this library, you may extend this exception to your version
 * of the library, but you are not obligated to do so.  If you do not wish
 * to do so, delete this exception statement from your version.
 */
package phyloviz.nj;

import phyloviz.nj.tree.NJRoot;
import phyloviz.project.ProjectItem;
import phyloviz.upgmanjcore.output.Format;

public class NeighborJoiningItem implements ProjectItem{

    private final NJRoot root;
    private final MEClusteringMethod cm;

    public NeighborJoiningItem(NJRoot root, MEClusteringMethod cm) {
        this.cm = cm;
        this.root = root;

    }
    public NJRoot getRoot() {
        return root;
    }

    @Override
    public String getFactoryName() {
        return "NJItemFactory";
    }

    @Override
    public String getOutput(Format f) {
    	String output = f.format(this, root);
        return output;
    }

    @Override
    public String toString() {
        return "Neighbor-Joining " + cm.toString();
    }
    @Override
    public String getMethodProviderName() {
        return cm.toString().split(" ")[0].toLowerCase();
    }
    @Override
    public String getAlgorithmLevel(){
        return "0";
    }
}
