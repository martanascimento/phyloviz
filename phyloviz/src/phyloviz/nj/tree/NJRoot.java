/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.nj.tree;

import phyloviz.matrix.OTU;
import phyloviz.upgmanjcore.output.INodeSaver;

/**
 *
 * @author Adriano
 */
public class NJRoot extends NJUnionNode implements INodeSaver{
         
    public final OTU n1, n2;
    public final float distance;

    public NJRoot(NJNode n1, NJNode n2, float distance){
    	super(-1, "root", n1, n2, distance, distance);
        this.n1 = n1;
        this.n2 = n2;
        this.distance = distance;
    }

	@Override
	public Object get() {
		return this;
	}
	public OTU getNodeLeft(){
        return n1;
    }
    public OTU getNodeRight(){
        return n2;
    }
    public String getDisplayName(){
        return "root";
    }
    public float getDistance(){
        return distance;
    }

	@Override
	public String toString() {
		return getDisplayName();
	}

}
