package phyloviz.nj.tree;

import phyloviz.matrix.Identifiable;

public class NJLeafNode extends NJNode{
	
	private Identifiable xis;
	
	public NJLeafNode(Identifiable xis) {
		super(xis.getUID(), xis.getID());
		this.xis = xis;
		
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public Identifiable getIdentifiable() {
		return this.xis;
	}

}
