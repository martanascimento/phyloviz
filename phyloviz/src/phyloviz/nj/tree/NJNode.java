package phyloviz.nj.tree;

import java.util.List;

import phyloviz.matrix.OTU;
import phyloviz.nj.algorithm.NJ;
import phyloviz.upgmanjcore.tree.Index;

public abstract class NJNode extends OTU{
	
	public int minIndex = -1;
	public float minQ = Float.POSITIVE_INFINITY;
	public float sum = 0;
	
	public NJNode(int uid, String id){
		super(uid, id);
	}

	public void updateSum(List<NJNode> nodes){
		Index curr = indexes.next;
		while(curr.index != -1) curr = curr.next;
		curr = curr.next;
		
		while(curr.index != index){
			sum += nodes.get(curr.index).getDistance(index);
			curr = curr.next;
		}
		
	}
	@Override
	public void setDistance(int i, float value) {
		super.setDistance(i, value);
		sum += value;
	}
	public void setDistance(int posL, int posR, float value) {
		super.setDistance(posL, value);
		sum = sum - distances[posL - offset] - distances[posR - offset] + value;
	}
	public void removeDistance(int minC, int minL) {
		sum = sum - distances[minC - offset] - distances[minL - offset];
	}
	
	public void calculateQ(NJ nj) {
        Index in = indexes.next;
        minQ  = Float.POSITIVE_INFINITY;
        while(in.index != -1){
            int l = in.index;
            float res = nj.calculateQ(index, l);
            if(res < minQ){
                minIndex = in.index;
                minQ = res;
            }
            in = in.next;
        }
	}

	public float getDistancesSum() {
		return sum;
	}

	public void updateSum(float f) {
		sum += f;
	}
	
	
	
	public abstract boolean isLeaf();
	public abstract int size();
}
