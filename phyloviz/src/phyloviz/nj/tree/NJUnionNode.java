package phyloviz.nj.tree;

import phyloviz.matrix.Identifiable;

public class NJUnionNode extends NJNode{
	
	public NJNode t1, t2;
	public float distance1, distance2;
	
	public NJUnionNode(int uid, String id, NJNode t1, NJNode t2, float distance1, float distance2) {
		super(uid, id);
		this.t1 = t1;
		this.t2 = t2;
		this.distance1 = distance1;
		this.distance2 = distance2;
	}

	public float getDistanceLeft() {
		return distance1;
	}
	public float getDistanceRight() {
		return distance2;
	}
	public float getDistance(NJNode n) {
		return n.getUID() == t1.getUID() ? distance1 : distance2;
	}
	@Override
	public boolean isLeaf() {
		return false;
	}
	
	@Override
	public int size() {
		return t1.size() + t2.size();
	}

	@Override
	public Identifiable getIdentifiable() {
		// TODO Auto-generated method stub
		return null;
	}
}