package phyloviz.upgmanjcore.tree;

public class Index {

    public final int index;
    public Index next;
    public Index prev;

    public Index(int idx) {
        this.index = idx;
        this.next = this.prev = this;
    }

    /**
     * Removes current IndexNode from IndexListNode
     */
    public void remove() {
        this.prev.next = this.next;
        this.next.prev = this.prev;
    }
}