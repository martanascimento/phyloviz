/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.upgmanjcore.output.newick;

import phyloviz.goeburst.GOeBurstResult;
import phyloviz.goeburst.output.newick.GOeBurstNewickWriter;
import phyloviz.goeburst.output.newick.MSTNewickWriter;
import phyloviz.goeburst.tree.GOeBurstMSTResult;
import phyloviz.nj.NeighborJoiningItem;
import phyloviz.nj.output.newick.NJNewickWriter;
import phyloviz.upgma.UPGMAItem;
import phyloviz.upgma.output.newick.UPGMANewickWriter;
import phyloviz.upgmanjcore.output.Format;

public class NewickFormat extends Format {
	
	static {
		writers.put(UPGMAItem.class, new UPGMANewickWriter());
		writers.put(GOeBurstResult.class, new GOeBurstNewickWriter());
		writers.put(GOeBurstMSTResult.class, new MSTNewickWriter());
		writers.put(NeighborJoiningItem.class, new NJNewickWriter());
	}

	public String getExtension() {
		return ".nwk";
	}

}
