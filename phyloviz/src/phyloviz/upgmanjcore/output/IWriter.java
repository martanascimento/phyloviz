package phyloviz.upgmanjcore.output;

public interface IWriter {
	
	public String write(INodeSaver n);

}
