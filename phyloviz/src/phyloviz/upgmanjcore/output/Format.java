package phyloviz.upgmanjcore.output;

import java.util.HashMap;
import java.util.Map;

import phyloviz.project.ProjectItem;

public abstract class Format {
	
	protected static final Map<Class<? extends ProjectItem>, IWriter> writers = new HashMap<Class<? extends ProjectItem>, IWriter>();
	
	public String format(ProjectItem item, INodeSaver n){
		IWriter w = writers.get(item.getClass());
		String output = w.write(n);
		return output;
	}

	public abstract String getExtension();
	
}
