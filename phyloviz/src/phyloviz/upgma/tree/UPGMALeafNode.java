package phyloviz.upgma.tree;

import phyloviz.matrix.Identifiable;

public class UPGMALeafNode extends UPGMANode{
	
	private Identifiable identifiable;
	
	public UPGMALeafNode(Identifiable identifiable) {
		super(identifiable.getUID(), identifiable.getID());
		this.identifiable = identifiable;
		
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public Identifiable getIdentifiable() {
		return this.identifiable;
	}

	public String toString(){
		return this.getID();
	}
}
