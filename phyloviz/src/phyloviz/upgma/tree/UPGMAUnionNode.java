package phyloviz.upgma.tree;

import phyloviz.matrix.Identifiable;

public class UPGMAUnionNode extends UPGMANode{
	
	public UPGMANode t1, t2;
	public float distance;
	
	public UPGMAUnionNode(int uid, String id, UPGMANode t1, UPGMANode t2, float distance) {
		super(uid, id);
		this.t1 = t1;
		this.t2 = t2;
		this.distance = distance;
	}

	public float getDistance() {
		return distance;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}
	
	@Override
	public int size() {
		return t1.size() + t2.size();
	}

	@Override
	public Identifiable getIdentifiable() {
		return null;
	}

	public UPGMANode getU() {
		return t1;
	}
	public UPGMANode getV() {
		return t2;
	}
	public String toString(){
		return "(" + t1.toString() + "," + t2.toString() + "): " + distance*2;
	}
}