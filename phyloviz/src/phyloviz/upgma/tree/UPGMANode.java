package phyloviz.upgma.tree;

import phyloviz.matrix.OTU;
import phyloviz.upgmanjcore.tree.Index;

public abstract class UPGMANode extends OTU{
	
	public int minIndex = -1;
	public float minDistance = Float.POSITIVE_INFINITY;

	public UPGMANode(int uid, String id) {
		super(uid, id);
	}
	
	@Override
	public void setDistance(int i, float value) {
		super.setDistance(i, value);
		if(value < minDistance){
            minIndex = i;
            minDistance = value;
        }
	}
	
	public void updateMin(UPGMAUnionNode union) {
		if(union.t1.index == minIndex || union.t2.index == minIndex || minIndex == -1){
			update(union.t2.index);
		}
	}
	private void update(int indexToIgnore){
		minIndex = -1;
		minDistance = Float.POSITIVE_INFINITY;
		Index next = indexes.next;
		while(next.index != -1){
			
			if(next.index == indexToIgnore){
				next = next.next;
				continue;
			}
			
			float d = getDistance(next.index);
			if(d < minDistance){
				minIndex = next.index;
				minDistance = d;
			}
			next = next.next;
		}
	}
	
	public abstract boolean isLeaf();
	public abstract int size();
}
