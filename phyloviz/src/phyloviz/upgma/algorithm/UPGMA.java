/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.upgma.algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import phyloviz.matrix.Identifiable;
import phyloviz.matrix.Matrix;
import phyloviz.matrix.TaxaPair;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.upgma.GCPClusteringMethod;
import phyloviz.upgma.tree.UPGMALeafNode;
import phyloviz.upgma.tree.UPGMANode;
import phyloviz.upgma.tree.UPGMARoot;
import phyloviz.upgma.tree.UPGMAUnionNode;

public class UPGMA {

	private final GCPClusteringMethod method;
	private Matrix<UPGMANode> m;
	private int unionID;

	public UPGMA(GCPClusteringMethod method, PairwiseDissimilarity<Identifiable> pd) {
		this.unionID = pd.size();
		this.method = method;
		
		List<UPGMANode> nodes = new ArrayList<>();
		for (int i = 0; i < pd.size(); i++) {
			nodes.add(new UPGMALeafNode(pd.get(i)));
		}
		this.m = new Matrix<UPGMANode>(nodes, pd, null);

	}			

	public UPGMARoot execute() {

		while (m.size() > 2) {

			TaxaPair<UPGMANode> pair = select();
			float distance = m.getDistance(pair.getU(), pair.getV());

			UPGMAUnionNode union = join(pair, distance);
			m.replace(union.t1, union);

			reduce(union);
			m.remove(union.t2);
		}

		TaxaPair<UPGMANode> pair = select();
		float distance = m.getDistance(pair.getU(), pair.getV());

		UPGMAUnionNode union = join(pair, distance);
		return new UPGMARoot(union.t1, union.t2, union.getDistance());
	}

	private TaxaPair<UPGMANode> select() {
		float minDistance = Float.POSITIVE_INFINITY;
		TaxaPair<UPGMANode> min = null;

		Iterator<UPGMANode> iter = m.iterator();
		while (iter.hasNext()) {

			UPGMANode t = (UPGMANode) iter.next();
			if (t.minDistance < minDistance) {
				min = new TaxaPair<UPGMANode>(t, m.get(t.minIndex), t.minDistance);
				minDistance = t.minDistance;
			}
		}
		return min;
	}

	private UPGMAUnionNode join(TaxaPair<UPGMANode> pair, float distance) {
		UPGMANode t1 = (UPGMANode) pair.getU();
		UPGMANode t2 = (UPGMANode) pair.getV();
		UPGMAUnionNode union = new UPGMAUnionNode(unionID, String.valueOf(unionID), t1, t2, distance / 2);
		unionID++;
		return union;
	}

	private void reduce(UPGMAUnionNode union) {
		UPGMANode t1 = (UPGMANode) union.t1;
		UPGMANode t2 = (UPGMANode) union.t2;
		Iterator<UPGMANode> it = m.iterator();
		while (it.hasNext()) {
			UPGMANode curr = (UPGMANode) it.next();
			if (curr.getUID() == union.getUID() || curr.getUID() == t1.getUID() || curr.getUID() == t2.getUID())
				continue;

			float d1 = m.getDistance(t1, curr);
			float d2 = m.getDistance(t2, curr);

			float distance = method.getLinkageCriteria(t1, t2, d1, d2);

			m.setDistance(union, curr, distance);
			curr.updateMin(union);

		}
	}
}
