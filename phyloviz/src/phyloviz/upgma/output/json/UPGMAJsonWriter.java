package phyloviz.upgma.output.json;

import java.util.LinkedHashMap;
import java.util.Map;

import phyloviz.matrix.OTU;
import phyloviz.upgma.tree.UPGMALeafNode;
import phyloviz.upgma.tree.UPGMARoot;
import phyloviz.upgma.tree.UPGMAUnionNode;
import phyloviz.upgmanjcore.output.INodeSaver;
import phyloviz.upgmanjcore.output.json.JsonWriter;

public class UPGMAJsonWriter extends JsonWriter{

	private StringBuilder leaves = new StringBuilder();
	private StringBuilder unions = new StringBuilder();
	
	
	@Override
	protected Map<String, String> getObjects(INodeSaver n) {
		Map<String, String> map = new LinkedHashMap<>();
		UPGMARoot root = (UPGMARoot) n.get();
		
		write(root.getNodeLeft());
		write(root.getNodeRight());
		
        map.put("leaf", leaves.toString());
        map.put("union", unions.toString());
        map.put("root", format(root));
        return map;
	} 
	
	
	private void add(StringBuilder sb, String s){
		sb.append(s);
	}
	private String format(UPGMARoot root){
		String l = root.getNodeLeft().getID();
		String r = root.getNodeRight().getID();
		return "\t\t{\"distance\": "+ root.getDistance() + ", \"left\": \"" + l + "\", \"right\": \"" + r + "\"},\n";
	}
	private void write(OTU n) {
		if (!n.isLeaf()) {
			UPGMAUnionNode node = (UPGMAUnionNode) n;
			OTU l = node.t1;
			OTU r = node.t2;
			
			write(l); write(r);
			add(unions, format(node));

		} else {
			UPGMALeafNode node = (UPGMALeafNode) n;
			add(leaves,format(node));
		}
	}
    private String format(UPGMALeafNode leaf){
    	String id = leaf.getID();
    	String profile = leaf.getIdentifiable().getID();
        return "\t\t{\"id\": \"" + id + "\", \"profile\": \"" + profile + "\"},\n";
    }
    private String format(UPGMAUnionNode union){
    	String id = union.getID();
    	float distance = union.getDistance();
    	String leftId = union.t1.getID();
    	String rightId = union.t2.getID();
        return "\t\t{\""
                + "id\": \"" + id + "\", \""
                + "distance\": " + distance + ", \""
                + "leftID\": \"" + leftId + "\", \""
                + "rightID\": \"" + rightId + "\"},\n";
        
    }  
}
