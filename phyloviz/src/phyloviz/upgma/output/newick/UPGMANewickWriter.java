package phyloviz.upgma.output.newick;

import phyloviz.matrix.OTU;
import phyloviz.upgma.tree.UPGMALeafNode;
import phyloviz.upgma.tree.UPGMARoot;
import phyloviz.upgma.tree.UPGMAUnionNode;
import phyloviz.upgmanjcore.output.INodeSaver;
import phyloviz.upgmanjcore.output.newick.NewickWriter;

public class UPGMANewickWriter extends NewickWriter {

	private UPGMARoot root;

	@Override
	public String write(INodeSaver n) {
		this.root = (UPGMARoot) n.get();

		OTU l = root.getNodeLeft();
		OTU r = root.getNodeRight();

		StringBuilder sb = new StringBuilder();

		sb.append("(").append(format(root, l)).append(",").append(format(root, r)).append(");");

		return sb.toString();
	}

	private String format(OTU parent, OTU n) {
		if (!n.isLeaf()) {
			UPGMAUnionNode node = (UPGMAUnionNode) n;
			OTU l = node.t1;
			OTU r = node.t2;

			StringBuilder sb = new StringBuilder();

			sb.append("(").append(format(n, l)).append(",").append(format(n, r)).append(")");
			sb.append(":").append(((UPGMAUnionNode)parent).getDistance() - node.getDistance()).toString();

			return sb.toString();
		} else {
			return ((UPGMALeafNode) n).getID() + ":" + ((UPGMAUnionNode)parent).getDistance();
		}
	}

}
