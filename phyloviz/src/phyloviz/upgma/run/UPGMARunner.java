/*-
 * Copyright (c) 2016, PHYLOViZ Team <phyloviz@gmail.com>
 * All rights reserved.
 * 
 * This file is part of PHYLOViZ <http://www.phyloviz.net/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package phyloviz.upgma.run;

import phyloviz.core.data.AbstractProfile;
import phyloviz.core.data.TypingData;
import phyloviz.matrix.Identifiable;
import phyloviz.matrix.dissimilarity.PairwiseDissimilarity;
import phyloviz.project.ProjectItem;
import phyloviz.upgma.GCPClusteringMethod;
import phyloviz.upgma.UPGMAItem;
import phyloviz.upgma.algorithm.UPGMA;
import phyloviz.upgma.tree.UPGMARoot;


public class UPGMARunner{
    
    private final GCPClusteringMethod method;
	private PairwiseDissimilarity<Identifiable> pd;

    public UPGMARunner(GCPClusteringMethod method, PairwiseDissimilarity<Identifiable> pd){
        this.method = method;
        this.pd = pd;
    }

	public ProjectItem run(TypingData<? extends AbstractProfile> td) {
        
        UPGMA algorithm = new UPGMA(method, pd);
        
        UPGMARoot root = algorithm.execute();
       
        return new UPGMAItem(root, method);
    }
    
}
